'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "b18a32260c28b8a467732b5088a76439",
"assets/FontManifest.json": "c7da84f17df71dd946690f55a6c63926",
"assets/fonts/MaterialIcons-Regular.otf": "7e7a6cccddf6d7b20012a548461d5d81",
"assets/NOTICES": "cdd0b1df4d779f4669728d2769ca841d",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/font_awesome_flutter/lib/fonts/fa-brands-400.ttf": "d1722d5cf2c7855862f68edb85e31f88",
"assets/packages/font_awesome_flutter/lib/fonts/fa-regular-400.ttf": "613e4cc1af0eb5148b8ce409ad35446d",
"assets/packages/font_awesome_flutter/lib/fonts/fa-solid-900.ttf": "dd3c4233029270506ecc994d67785a37",
"assets/res/fonts/Montserrat-Bold.otf": "9c71d42b6a840ecfda8fc555040a1c76",
"assets/res/fonts/Montserrat-Regular.otf": "92db9a0772b3732e6d686fec3711af42",
"assets/res/icons/behance.svg": "35ad2d47e647d0b168e7707b2984c6b5",
"assets/res/icons/check.svg": "4220c82511cc1dfb40b8bba7d25c5f55",
"assets/res/icons/download.svg": "628700a3031424d215a441fab2da1731",
"assets/res/icons/dribble.svg": "d392567c5678d42472d2c7b766268101",
"assets/res/icons/github.svg": "9226aa209923e38c0a6ddcb236e2bc68",
"assets/res/icons/instagram.svg": "34e1d759d3dd10228152921064e467ac",
"assets/res/icons/linkedin.svg": "5b2195ddf9e879047dd8a163c4194920",
"assets/res/icons/twitter.svg": "a4a0163fef48a4247a305528c07bc4fa",
"assets/res/images/email.png": "5eb3c4b86aafbee72b8c471b29413a50",
"assets/res/images/figma.png": "ac00fa7b6768286ad44283e4595dd07e",
"assets/res/images/ipb.png": "3243904a51032278476b8c39025ee0dd",
"assets/res/images/js.png": "c502658a509d27b53679b3ef73c0d82f",
"assets/res/images/mappin.png": "9cc090022ae31337336d2024160714b8",
"assets/res/images/me.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/metapp.png": "0a3b61c31b655ce4c1fbb526ca847a1b",
"assets/res/images/phone.png": "45903a1ffa9ede882171aca9f71c4c29",
"assets/res/images/py.png": "9187b2f4719ee4fbe75ce6fbaac9f4b2",
"assets/res/images/scrum.png": "0356b119029f646021c5fefee47c8852",
"assets/res/images/self.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/self1.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/self2.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/self3.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/self4.png": "3b3eb9b68fe416778679cedf51486272",
"assets/res/images/self5.png": "ff958909898bdfab57b1df81b8134d4e",
"assets/res/images/splash_logo.png": "e39f1158059d77f27d97717f171c1e68",
"assets/res/images/teco.png": "9411015b7d44014a014b14d5af8a74a8",
"assets/res/images/vuejs.png": "7aabd127b80bebf5dad3b0c9e07c0a97",
"assets/res/images/whatsapp.png": "426617ad28567da23a2346566d84b5a6",
"assets/res/images/xd.png": "52738253b69dc8365ab72d2f9f38e58e",
"canvaskit/canvaskit.js": "c2b4e5f3d7a3d82aed024e7249a78487",
"canvaskit/canvaskit.wasm": "4b83d89d9fecbea8ca46f2f760c5a9ba",
"canvaskit/profiling/canvaskit.js": "ae2949af4efc61d28a4a80fffa1db900",
"canvaskit/profiling/canvaskit.wasm": "95e736ab31147d1b2c7b25f11d4c32cd",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icon.png": "a6c13dacf832058f2b2f8651727daa94",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"index.html": "3a3c8a358da39785b60f1497a567a5c8",
"/": "3a3c8a358da39785b60f1497a567a5c8",
"main.dart.js": "c891d3b404b3ad8a22fbf45df06b0c6f",
"manifest.json": "d4c65d9c4bd0dea327f1f98b10257646",
"splash/img/dark-1x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/dark-2x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/dark-3x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/dark-4x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/light-1x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/light-2x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/light-3x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/img/light-4x.png": "a6c13dacf832058f2b2f8651727daa94",
"splash/style.css": "1bc5c7ef6d3b73839eede3034d09a8a9",
"version.json": "52115a4958bc3e735120b80b627630dd"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
