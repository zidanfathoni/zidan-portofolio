import 'package:flutter/material.dart';
import 'package:web_porto/ui/components/app.dart';
import 'package:web_porto/ui/components/education.dart';
import 'package:web_porto/ui/components/skill.dart';
import 'package:web_porto/ui/constant.dart';
import 'dart:html' as html;

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../components/contact.dart';
import '../project/project.dart';
import '../components/profile.dart';
import '../widgets/custom_button.dart';

class ProjectScreen extends StatelessWidget {
  const ProjectScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colorBg,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              children: <Widget>[
                SelectableText(
                  'PROJECTS',
                  style: headText,
                ),
                const SizedBox(
                  height: 50,
                ),
                //Metrik App
                const AppSection(
                  imageFirst: true,
                  imagePath: 'res/images/metapp.png',
                  platformName: 'ANDROID APP',
                  appName: 'METRIK 2019',
                  gitUrl: 'https://github.com/fauzanafism/metrik2019_flutter',
                  description:
                      'An Android app for Meteorologi Interaktif 2019\nnational competition',
                ),
                const SizedBox(
                  height: 50,
                ),
                //Teco
                const AppSection(
                    imageFirst: false,
                    imagePath: 'res/images/teco.png',
                    platformName: 'DESKTOP APP',
                    gitUrl: 'https://github.com/fauzanafism/TECOs',
                    appName: 'TECO',
                    description:
                        'A desktop app for estimating corn production \nbased on meteorological variable such as \ntemperature'),
              ],
            ),
          ),
        ));
  }
}
