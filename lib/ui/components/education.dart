import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:web_porto/ui/constant.dart';

class Education extends StatelessWidget {
  const Education({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      height: 450,
      // width: 1020,
      // color: Colors.red.withOpacity(0.1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SelectableText(
            'EDUCATION',
            style: headText,
          ),
          const SizedBox(
            height: 15,
          ),
          SelectableText(
            'Bachelor Degree with specialization in Software Development and UI application design.',
            style: bodyText,
          ),
          const SizedBox(
            height: 35,
          ),
          ResponsiveRowColumn(
            rowMainAxisAlignment: MainAxisAlignment.center,
            layout: ResponsiveWrapper.of(context).isSmallerThan('TABLET2')
                ? ResponsiveRowColumnType.COLUMN
                : ResponsiveRowColumnType.ROW,
            children: [
              const ResponsiveRowColumnItem(
                child: SizedBox(
                  width: 30,
                ),
              ),
              ResponsiveRowColumnItem(
                  child: SizedBox(
                      height: 140,
                      child: Image(image: AssetImage('res/images/ipb.png')))),
              const ResponsiveRowColumnItem(
                  child: SizedBox(
                width: 30,
                height: 14,
              )),
              ResponsiveRowColumnItem(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SelectableText(
                      'Amikom Purwokerto University',
                      style: titleText.copyWith(
                          color: Color.fromARGB(255, 150, 0, 138)),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SelectableText(
                      'Information Systems',
                      style: titleText.copyWith(
                          color: Color.fromARGB(255, 150, 0, 138)),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SelectableText(
                      '2018 - 2022',
                      style: titleText.copyWith(
                          color: Color.fromARGB(255, 150, 0, 138)),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SelectableText(
                      'GPA: 3.66',
                      style: titleText.copyWith(
                          color: Color.fromARGB(255, 150, 0, 138)),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
