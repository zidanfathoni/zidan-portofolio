import 'package:flutter/material.dart';
import 'dart:html' as html;
import '../constant.dart';

class AppbarCustom extends StatelessWidget {
  const AppbarCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
      title: const SizedBox(
          width: 70,
          child: ClipRRect(
            child: Image(
              image: AssetImage('res/images/splash_logo.png'),
            ),
          )),
      actions: [
        TextButton.icon(
          // <-- TextButton
          onPressed: () {},
          icon: const Icon(
            Icons.code,
            size: 15.0,
            color: Colors.white,
          ),
          label: const Text(
            'Project',
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
            ),
          ),
        ),
        TextButton.icon(
          // <-- TextButton
          onPressed: () {},
          icon: const Icon(
            Icons.people,
            size: 15.0,
            color: Colors.white,
          ),
          label: const Text(
            'Profile',
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
            ),
          ),
        ),
      ],
    );
  }
}
