import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:web_porto/ui/constant.dart';

class Experience extends StatelessWidget {
  const Experience({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'EXPERIENCE',
                  style: headText,
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SelectableText(
                  'Breezelabs - Banyumas, Indonesia',
                  style: secondheadText,
                ),
                SelectableText(
                  'Full Stack Developer-Freelance (January - May 2022)',
                  style: thirdheadText,
                ),
                const SizedBox(
                  height: 5,
                ),
                SelectableText(
                  '- Become a Developer who completes the project of making and developing 2 website applications and 3 mobile applications with a frontend using Flutter for mobile, VueJS for website and a backend using AdonisJS.',
                  style: bodyText,
                ),
                SelectableText(
                  '- Become a Scrum Master in a team that uses Agile SCRUM methods to complete projects.',
                  style: bodyText,
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SelectableText(
                  'Breezelabs - Banyumas, Indonesia',
                  style: secondheadText,
                ),
                SelectableText(
                  'UI Designer-Freelance (January - May 2022)',
                  style: thirdheadText,
                ),
                const SizedBox(
                  height: 5,
                ),
                SelectableText(
                  '- Become a UI Designer for Mobile Applications for a travel service company, child talent service company and website admin application.',
                  style: bodyText,
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SelectableText(
                  'CV. KUSUMA BUANA - Banyumas, Indonesia',
                  style: secondheadText,
                ),
                SelectableText(
                  'Full Stack Developer-Intern (August 2021 - January 2022)',
                  style: thirdheadText,
                ),
                const SizedBox(
                  height: 5,
                ),
                SelectableText(
                  '- Become a developer working on a mobile application creation and development project with a frontend using Flutter and a backend using AdonisJS.',
                  style: bodyText,
                ),
                SelectableText(
                  '- Become a Scrum Master in a team that uses Agile SCRUM methods to complete projects.',
                  style: bodyText,
                ),
                SelectableText(
                  '- I managed to launch the application and get the HAKI of the project that was created.',
                  style: bodyText,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
