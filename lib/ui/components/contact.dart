import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:web_porto/models/contact_item.dart';

import '../widgets/custom_button.dart';

class Contact extends StatelessWidget {
  const Contact({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 30),
        padding: const EdgeInsets.only(bottom: 80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                // ResponsiveVisibility(
                //   hiddenWhen: [Condition.smallerThan(name: 'TABLET2')],
                //   child: ContactItem(
                //       iconPath: 'res/images/mappin.png',
                //       title: 'ADDRESS',
                //       text1: 'Ciomas, Bogor'),
                // ),
                ContactItem(
                    iconPath: 'res/images/mappin.png',
                    title: 'ADDRESS',
                    text1: 'Banyumas, Indonesia'),

                ContactItem(
                    iconPath: 'res/images/email.png',
                    title: 'EMAIL',
                    text1: 'zidhaanb@gmail.com'),
              ],
            ),
            SizedBox(height: 50),
            Padding(
                padding: EdgeInsets.all(8.0),
                child: CustomButton(
                  urlPath: 'mailto:zidhaanb@gmail.com',
                  text: 'HIRE ME',
                )),
          ],
        ),
      ),
    );
  }
}
