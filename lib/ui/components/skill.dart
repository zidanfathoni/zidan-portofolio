import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:web_porto/ui/constant.dart';

class Skills extends StatelessWidget {
  const Skills({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Container(
        width: 500,
        height: 400,
        child: Column(
          children: [
            SelectableText(
              'SKILLS',
              style: headText,
            ),
            const SizedBox(
              height: 35,
            ),
            ResponsiveRowColumn(
              rowMainAxisAlignment: MainAxisAlignment.center,
              layout: ResponsiveWrapper.of(context).isSmallerThan('TABLET2')
                  ? ResponsiveRowColumnType.COLUMN
                  : ResponsiveRowColumnType.ROW,
              children: [
                const ResponsiveRowColumnItem(
                  child: SizedBox(
                    width: 8,
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: const FlutterLogo(
                          size: 65,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: SizedBox(
                            width: 65, child: Image.asset('res/images/js.png')),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: const SizedBox(
                            width: 65,
                            child: Image(
                                image: AssetImage('res/images/vuejs.png'))),
                      )
                    ],
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: SizedBox(
                            width: 65, child: Image.asset('res/images/xd.png')),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: const SizedBox(
                            width: 65,
                            child: Image(
                                image: AssetImage('res/images/figma.png'))),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: const SizedBox(
                            width: 65,
                            child: Image(
                                image: AssetImage('res/images/scrum.png'))),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
