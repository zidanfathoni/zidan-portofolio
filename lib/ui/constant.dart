import 'package:flutter/material.dart';

const Color colorBg = Color.fromARGB(255, 0, 0, 0);
const Color colorAppBar = Color(0xff161515);
const Color colorButton = Color.fromARGB(255, 1, 156, 25);

TextStyle headText = const TextStyle(
    fontSize: 27,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Montserrat');

TextStyle secondheadText = const TextStyle(
    fontSize: 24,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Montserrat');

TextStyle thirdheadText = const TextStyle(
    fontSize: 23, color: Colors.white, fontFamily: 'Montserrat');

TextStyle titleText = const TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Montserrat');

TextStyle secondText = const TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Montserrat');

TextStyle bodyText =
    const TextStyle(fontSize: 16, color: Colors.grey, fontFamily: 'Montserrat');
