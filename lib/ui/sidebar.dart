import 'package:easy_sidemenu/easy_sidemenu.dart';
import 'package:flutter/material.dart';
import 'package:web_porto/ui/components/education.dart';
import 'package:web_porto/ui/components/skill.dart';

import 'package:web_porto/ui/constant.dart';
import 'dart:html' as html;

import 'package:easy_sidemenu/easy_sidemenu.dart';

import 'components/contact.dart';
import 'components/experience.dart';
import 'components/profile.dart';
import 'profile/profileScreen.dart';
import 'project/project.dart';
import 'widgets/custom_button.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SidebarScreen extends StatefulWidget {
  const SidebarScreen({Key? key}) : super(key: key);

  @override
  _SidebarScreenState createState() => _SidebarScreenState();
}

class _SidebarScreenState extends State<SidebarScreen> {
  PageController page = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBg,
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SideMenu(
            controller: page,
            style: SideMenuStyle(
                openSideMenuWidth: 200,
                displayMode: SideMenuDisplayMode.auto,
                hoverColor: Colors.blue[100],
                selectedColor: Color.fromARGB(255, 0, 0, 0),
                selectedTitleTextStyle: TextStyle(color: Colors.white),
                selectedIconColor: Colors.white,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                backgroundColor: Colors.blueGrey[50]),
            title: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 170,
                      maxWidth: 170,
                    ),
                    child: Image.asset(
                      'res/images/splash_logo.png',
                    ),
                  ),
                  Divider(
                    indent: 8.0,
                    endIndent: 8.0,
                  ),
                ],
              ),
            ),
            items: [
              SideMenuItem(
                priority: 0,
                title: 'Home',
                onTap: () {
                  page.jumpToPage(0);
                },
                icon: Icon(Icons.home),
              ),
              SideMenuItem(
                priority: 1,
                title: 'Project',
                onTap: () {
                  page.jumpToPage(1);
                },
                icon: Icon(Icons.code),
              ),
              SideMenuItem(
                priority: 2,
                title: 'Education',
                onTap: () {
                  page.jumpToPage(2);
                },
                icon: Icon(FontAwesomeIcons.bookOpenReader),
              ),
              SideMenuItem(
                priority: 3,
                title: 'Skill',
                onTap: () {
                  page.jumpToPage(3);
                },
                icon: Icon(FontAwesomeIcons.screwdriverWrench),
              ),
              SideMenuItem(
                priority: 4,
                title: 'Experience',
                onTap: () {
                  page.jumpToPage(4);
                },
                icon: Icon(Icons.work),
              ),
              SideMenuItem(
                priority: 5,
                title: 'Contact',
                onTap: () {
                  page.jumpToPage(5);
                },
                icon: Icon(Icons.phone),
              ),
            ],
          ),
          Expanded(
            child: PageView(
              controller: page,
              children: [
                MyProfile(key: PageStorageKey('Intro')),
                ProjectScreen(),
                Education(),
                Skills(),
                Experience(),
                Contact(),
              ],
            ),
          ),
        ],
      ),
      persistentFooterButtons: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "©2022 Muhamad Zidan Fathoni. All rights reserved.",
                    style: bodyText,
                  ),
                ),
              ],
            ),
          ],
        )
      ],
    );
  }
}
