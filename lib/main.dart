import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:web_porto/ui/components/education.dart';

import 'ui/components/contact.dart';
import 'ui/components/profile.dart';
import 'ui/components/skill.dart';
import 'ui/components/themeClass.dart';
import 'ui/project/project.dart';
import 'ui/sidebar.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      darkTheme: ThemeClass.darkTheme,
      builder: (context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        breakpoints: const [
          ResponsiveBreakpoint.resize(350, name: MOBILE),
          ResponsiveBreakpoint.resize(450, name: 'MOBILE2'),
          ResponsiveBreakpoint.resize(550, name: 'TABLET2'),
          ResponsiveBreakpoint.autoScale(650, name: TABLET),
          ResponsiveBreakpoint.resize(750, name: DESKTOP),
          ResponsiveBreakpoint.resize(1080, name: 'DESKTOP2'),
          ResponsiveBreakpoint.autoScale(1650, name: 'XL'),
        ],
      ),
      home: const SidebarScreen(),
      routes: {
        '/project': (context) => const ProjectScreen(),
        '/Intro': (context) => const MyProfile(),
        '/homepage': (context) => const SidebarScreen(),
        '/education': (context) => const Education(),
        '/skills': (context) => const Skills(),
        '/contact': (context) => const Contact(),
      },
      initialRoute: "/",
    );
  }
}
